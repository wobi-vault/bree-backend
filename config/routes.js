/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {
  'GET /catalogs': 'CatalogsController.findAll',

  'POST /lead': 'LeadController.create',

  'PATCH /application/:id': 'ApplicationController.update',

  'POST /report/email': 'ReportController.email',
  'POST /report/:id/resend': 'ReportController.resend',
  'GET /report/columns': 'ReportController.columns',
  'GET /report': 'ReportController.find',
  'POST /report': 'ReportController.create',
  'DELETE /report/:id': 'ReportController.destroy',
  'PUT /report': 'ReportController.update',

  'GET /card': 'CardController.find',
  'POST /card': 'CardController.create',
  'DELETE /card/:id': 'CardController.destroy',
  'PUT /card': 'CardController.update',
};