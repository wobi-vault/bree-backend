/**
 *
 * LeadYears.js
 *
 */

module.exports = {
  attributes: {
    description: {
      type: Sequelize.STRING(60),
      required: true
    },
    active: {
      type: Sequelize.BOOLEAN,
      allowNull: false,
      defaultValue: true
    }
  },

  associations: () => {},

  options: {
    tableName: 'LeadYears',
    classMethods: {},
    instanceMethods: {},
    hooks: {}
  }
};


