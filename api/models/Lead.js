/**
 *
 * Lead.js
 *
 */

module.exports = {
  attributes: {
    name: {
      type: Sequelize.STRING(120),
      allowNull: false,
    },
    pSurname: {
      type: Sequelize.STRING(120),
      allowNull: false,
    },
    mSurname: {
      type: Sequelize.STRING(120),
      allowNull: false,
    },
    email: {
      type: Sequelize.STRING(120),
      allowNull: false,
    },
    phone: {
      type: Sequelize.STRING(10),
      allowNull: false,
    },
    rfc: {
      type: Sequelize.STRING(20),
      allowNull: false,
    },
    terms: {
      type: Sequelize.BOOLEAN,
      allowNull: false,
    },
    privacy: {
      type: Sequelize.BOOLEAN,
      allowNull: false
    },
  },

  associations: () => {},

  options: {
    tableName: 'Lead',
    classMethods: {},
    instanceMethods: {},
    hooks: {}
  }
};
