/**
 * Report.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  attributes: {
    title: {
      type: Sequelize.TEXT(),
      allowNull: true
    },
    description: {
      type: Sequelize.TEXT(),
      allowNull: true
    },
    hour: {
      type: Sequelize.INTEGER,
      allowNull: false
    },
    columns: {
      type: Sequelize.TEXT(),
      allowNull: false
    },
    emails: {
      type: Sequelize.TEXT(),
      allowNull: false
    },
    subject: {
      type: Sequelize.TEXT(),
      allowNull: true
    },
    repeatedData: {
      type: Sequelize.BOOLEAN,
      allowNull: false,
      defaultValue: false
    },
    active: {
      type: Sequelize.BOOLEAN,
      allowNull: false,
      defaultValue: true
    },
    startDate: {
      type: Sequelize.STRING(20),
      allowNull: false,
      defaultValue: false,
    },
    endDate: {
      type: Sequelize.STRING(20),
      allowNull: false,
      defaultValue: false,
    }
  },

  associations: function() {},

  options: {
    tableName: 'Report',
    classMethods: {},
    instanceMethods: {},
    hooks: {}
  }

};
