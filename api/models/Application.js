/**
 *
 * Application.js
 *
 */

module.exports = {
  attributes: {
    companyOwner: {
      type: Sequelize.STRING(2),
      allowNull: true,
    },
    companyName: {
      type: Sequelize.STRING(120),
      allowNull: true,
    },
  },

  associations: () => {
    Application.belongsTo(Lead, {
      as: 'lead',
      foreignKey: {
        name: 'leadId',
        allowNull: false
      }
    });
    Application.belongsTo(Sat, {
      as: 'sat',
      foreignKey: {
        name: 'satId',
        allowNull: true,
      },
    });
    Application.belongsTo(LeadYears, {
      as: 'leadYears',
      foreignKey: {
        name: 'leadYearsId',
        allowNull: true,
      },
    });
    Application.belongsTo(CompanyType, {
      as: 'companyType',
      foreignKey: {
        name: 'companyTypeId',
        allowNull: true,
      }
    });
    Application.belongsTo(CompanyWorkers, {
      as: 'companyWorkers',
      foreignKey: {
        name: 'companyWorkersId',
        allowNull: true,
      }
    });
    Application.belongsTo(CompanyYears, {
      as: 'companyYears',
      foreignKey: {
        name: 'companyYearsId',
        allowNull: true,
      }
    });
  },

  options: {
    tableName: 'Application',
    classMethods: {},
    instanceMethods: {},
    hooks: {}
  }
};
