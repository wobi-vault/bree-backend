/**
 * Card.js
 *
 */

module.exports = {
  attributes: {
    title: {
      type:Sequelize.STRING(250),
      required: true
    },
    description: {
      type:Sequelize.STRING(250)
    },
    image: {
      type: Sequelize.TEXT('long'),
      allowNull: false,
      validate: {
        is: {
          args: /^data:image\/([a-zA-Z]{3,4});base64,(?:[A-Za-z0-9+/]{4})*(?:[A-Za-z0-9+/]{2}==|[A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{4})$/g,
          msg: 'Imagen no permitida.'
        },
        notNull: {
          msg: 'La imagen es obligatoria.'
        },
        notEmpty: {
          msg: 'La imagen es obligatoria.'
        }
      }
    },
    url: {
      type:Sequelize.STRING(250),
      required: true
    },
    order: {
      type:Sequelize.INTEGER,
      required: true
    },
    active: {
      type: Sequelize.BOOLEAN,
      defaultsTo: true
    }
  },

  associations: () => {},

  options: {
    tableName: 'Card',
    classMethods: {},
    instanceMethods: {},
    hooks: {}
  }
};

