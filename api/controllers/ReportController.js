/**
 * ReportController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

const fs = require('fs');

module.exports = {
  find: async (req, res) => {
    const params = req.allParams();
    const active = params.active ? {
      active: params.active
    } : {};
    try {
      const report = await Report.findAll({
        where: active
      });
      return res.ok(report);
    } catch (err) {
      return res.serverError(err);
    }
  },

  create: async (req, res) => {
    const params = req.allParams();
    try {
      const report = await Report.create(params);
      return res.ok(report);
    } catch (err) {
      return res.serverError(err);
    }
  },

  update: async (req, res) => {
    const {
      id,
      ...rest
    } = req.allParams();
    try {
      const report = await Report.findOne({
        where: {
          id
        }
      });
      if (_.isNull(report)) {
        const error = new Error('Unable to find report');
        return res.badRequest(error);
      }
      const result = await report.update(rest);
      return res.ok(result);
    } catch (err) {
      return res.serverError(err);
    }
  },

  email: async (req, res) => {
    let params = req.allParams();
    console.log(params);
    try {
      if (!params.startDate || !params.endDate) {
        return res.badRequest({}, {
          message: 'One or more params missing.'
        });
      }

      let startDate = new Date(params.startDate);
      let endDate = new Date(params.endDate);
      let columns = params.columns || sails.config.report.columns;

      if (!startDate.getDate() || !endDate.getDate()) {
        return res.badRequest({}, {
          message: 'Not a valid date.'
        });
      }

      let leads = await sails.helpers.getReportData(params.startDate, params.endDate, columns, params.repeated);
      let report = await sails.helpers.buildReport(leads, columns);
      let message = `Reporte del ${startDate.toLocaleString('es')}
        al ${endDate.toLocaleString('es')}.
        Total de leads nuevos: ${report.count}.`;
      if (!params.repeated) {
        message =
          message +
          '\n Nota: El reporte omite los leads repetidos, basado en su correo electrónico y teléfono.';
      }

      let attachment = (report.count) ? [report.path] : null;

      let inputs = {
        subject: sails.config.report.subject,
        message: message,
        to: sails.config.report.to,
        attachments: attachment
      };

      await sails.helpers.sendEmail.with(inputs);
      console.log('Email sended');
      fs.unlink(report.path, err => {
        if (err) {
          console.log(report.path, ' wasn\'t deleted');
        } else {
          console.log(report.path, ' was deleted');
        }
      });
      await Report.create({
        title: params.title || sails.config.report.title || '',
        subject: sails.config.report.subject,
        hour: new Date().getHours(),
        columns: columns.join(','),
        repeatedData: params.repeated,
        emails: sails.config.report.to.join(','),
        startDate: params.startDate,
        endDate: params.endDate,
      });

      return res.ok(leads);

    } catch (e) {
      return res.negotiate(e);
    }
  },

  resend: async (req, res) => {
    let params = req.allParams();

    try {
      let report = await Report.findByPk(params.id);
      let columns = report.columns.split(',').map(c => {
        return c.trim();
      });
      let now = new Date();

      let startDate = new Date(now);
      startDate.setMinutes(0);
      startDate.setHours(report.hour || 0);
      startDate.setSeconds(0);
      startDate.setMilliseconds(0);
      let endDate = new Date(startDate);
      endDate.setDate(startDate.getDate() + 1);


      let leads = await sails.helpers.getReportData(startDate.toString().slice(0, 24), endDate.toString().slice(0, 24), columns, report.repeatedData);
      let reportDoc = await sails.helpers.buildReport(leads, columns);
      let message = `Reporte del ${startDate.toLocaleString('es')}
      al ${endDate.toLocaleString('es')}.
      Total de leads nuevos: ${reportDoc.count}.`;
      if (!report.repeatedData) {
        message =
          message +
          '\n Nota: El reporte omite los leads repetidos, basado en su correo electrónico y teléfono.';
      }

      let attachment = (reportDoc.count) ? [reportDoc.path] : null;

      let inputs = {
        subject: sails.config.report.subject,
        message: message,
        to: report.emails,
        attachments: attachment
      };
      await sails.helpers.sendEmail.with(inputs);
      console.log('Email sended');
      fs.unlink(reportDoc.path, err => {
        if (err) {
          console.log(reportDoc.path, ' wasn\'t deleted');
        } else {
          console.log(reportDoc.path, ' was deleted');
        }
      });

      return res.ok(leads);

    } catch (e) {
      return res.negotiate(e);
    }
  },

  columns: (req, res) => {
    return res.ok(sails.config.report.columns);
  }
};