/**
 *
 * LeadController.js
 *
 */

module.exports = {
  create: async (req, res) => {
    const params = req.allParams();
    try {
      const lead = await Lead.create(params);
      const app = await Application.create({ leadId: _.get(lead, 'id') });
      return res.ok({
        ..._.get(lead, 'dataValues', {}),
        applicationId: _.get(app, 'dataValues.id')
      });
    } catch(err) {
      return res.serverError(err);
    }
  }
};
