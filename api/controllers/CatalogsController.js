/**
 *
 * CatalogController.js
 *
 */

module.exports = {
  findAll: async (req, res) => {
    const validCatalogs = ['companyType', 'companyWorkers', 'companyYears', 'leadYears', 'sat'];
    const { isValid, catalog } = _.flow(
      obj => _.get(obj, 'allParams'),
      func => func(),
      params => _.get(params, 'catalog'),
      res => ({ isValid: _.includes(validCatalogs, res), catalog: res })
    )(req);
    if (!isValid) {
      const error = new Error('Invalid Query Parameters');
      return res.badRequest(error);
    }
    try {
      if (_.isEqual(catalog, 'companyType')) {
        const response = await CompanyType.findAll();
        return res.ok(response);
      }
      if (_.isEqual(catalog, 'companyWorkers')) {
        const response = await CompanyWorkers.findAll();
        return res.ok(response);
      }
      if (_.isEqual(catalog, 'companyYears')) {
        const response = await CompanyYears.findAll();
        return res.ok(response);
      }
      if (_.isEqual(catalog, 'leadYears')) {
        const response = await LeadYears.findAll();
        return res.ok(response);
      }
      if (_.isEqual(catalog, 'sat')) {
        const response = await Sat.findAll();
        return res.ok(response);
      }
    } catch (err) {
      return res.serverError(err);
    }
  }
};
