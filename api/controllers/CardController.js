/**
 *
 * CatalogController.js
 *
 */

module.exports = {
  find: async (req, res) => {
    const params = req.allParams();
    const active = params.active ? { active: params.active } : {};
    try {
      const card = await Card.findAll({
        where: active
      });
      return res.ok(card);
    } catch (err) {
      return res.serverError(err);
    }
  },
  create: async (req, res) => {
    const params = req.allParams();
    try {
      const card = await Card.create(params);
      return res.ok(card);
    } catch (err) {
      return res.serverError(err);
    }
  },
  update: async (req, res) => {
    const { id, ...rest } = req.allParams();
    console.log(id);
    try {
      const card = await Card.findOne({
        where: { id }
      });
      if (_.isNull(card)) {
        const error = new Error('Unable to find card');
        return res.badRequest(error);
      }
      const result = await card.update(rest);
      return res.ok(result);
    } catch (err) {
      return res.serverError(err);
    }
  },
  destroy: async (req, res) => {
    const { id } = req.allParams();
    try {
      const card = Card.destroy({
        where: { id }
      });
      return res.ok(card);
    } catch (err) {
      return res.serverError(err);
    }
  }
};
