/**
 *
 * ApplicationController.js
 *
 */

module.exports = {
  update: async (req, res) => {
    const { id, leadId, ...rest } = req.allParams();
    try {
      const app = await Application.findOne({
        where: { id, leadId },
      });
      if (_.isNull(app)) {
        const error = new Error('Unable to find lead or application');
        return res.badRequest(error);
      }
      const result = await app.update(rest);
      return res.ok(result);
    } catch(err) {
      return res.serverError(err);
    }
  }
};
