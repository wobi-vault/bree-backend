const nodemailer = require('nodemailer');
module.exports = {

  description: 'Get data from view full_lead',

  inputs: {
    startDate: {
      type: 'ref',
      description: 'The start date for query.',
      required: true
    },
    endDate: {
      type: 'ref',
      description: 'The end date for query.',
      required: true
    },
    columns: {
      type: 'ref',
      description: 'The columns for query.',
      required: true
    },
    repeated: {
      type: 'boolean',
      description: 'The value to know if include or not include repeated leads.',
      defaultsTo: false,
      required: false
    }
  },

  exits: {
    success: {},
    error: {}
  },

  fn: async (inputs, exits) => {
    try {
      const Sequelize = require('sequelize');
      const dbCredentials = sails.config.datastores.default;
      const sequelize = new Sequelize(dbCredentials.database, dbCredentials.user, dbCredentials.password, {
        host: dbCredentials.options.host,
        port: dbCredentials.options.port,
        dialect: dbCredentials.options.dialect
      });
      let columns = inputs.columns;
      columns = _.uniq(columns);
      columns = '"' + columns.join('","') + '"';

      let repeatedFilter = inputs.repeated ? '' : 'AND (SELECT COUNT(*) FROM "Lead" WHERE "Lead".email = "Correo" AND "Lead".phone = "Teléfono") = 1';
      let QUERY_VIEW = `SELECT ${columns} FROM public.full_lead
                        WHERE "Fecha de Registro" >= '${inputs.startDate}' AND "Fecha de Registro" < '${inputs.endDate}'
                        ${repeatedFilter}
                        LIMIT 99999`;

      sequelize.query(QUERY_VIEW, {
          type: sequelize.QueryTypes.SELECT
        })
        .then(leads => {
          return exits.success(leads);
        });
    } catch (error) {
      console.log(error);
      return exits.error(error);
    }
  }

};