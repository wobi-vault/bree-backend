const nodemailer = require('nodemailer');
module.exports = {

  description: 'Send an email according to inputs',

  inputs: {
    subject: {
      type: 'string',
      description: 'The subject for email.',
      required: true
    },
    message: {
      type: 'string',
      description: 'The content of the email, plain text or html.',
      required: true
    },
    to: {
      type: 'ref',
      description: 'Array of receivers to send email.',
      required: true
    },
    attachments: {
      type: 'ref',
      description: 'Array of attachments',
      required: false
    }
  },

  exits: {
    success: {},
    emailError: {}
  },

  fn: async (inputs, exits) => {
    try {
      var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
          user: sails.config.emailCredentials.email,
          pass: sails.config.emailCredentials.password
        }
      });

      var attachments = [];
      if (inputs.attachments && inputs.attachments.length) {
        attachments = _.map(inputs.attachments, (att) => {
          return {
            path: att
          };
        });
      }

      // Activar permiso de aplicaciones: https://myaccount.google.com/lesssecureapps
      const mailOptions = {
        from: sails.config.emailCredentials.email, // sender address
        to: inputs.to, // list of receivers
        subject: inputs.subject, // Subject line
        html: inputs.message, // plain text body
        attachments: attachments // attachment files
      };
      transporter.sendMail(mailOptions, (err, info) => {
        if (err) {
          return exits.emailError(err);
        }
        return exits.success();
      });
    } catch (error) {
      return exits.emailError(error);
    }
  }

};
